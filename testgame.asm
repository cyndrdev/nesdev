	.inesprg 1	 ; 1x 16KB PRG code
	.ineschr 1	 ; 1x	8KB CHR data
	.inesmap 0	 ; mapper 0 = NROM, no bank swapping
	.inesmir 1	 ; background mirroring
	

;;;;;;; VARIABLES ;;;;;;;;

	.rsset $0000
pointer_lo			.rs 1
pointer_hi			.rs 1
buttons 			.rs 1
buttons_old			.rs 1
buttons_pressed		.rs 1
buttons_released	.rs 1
turn_anim_timer		.rs 1
lfsr_index			.rs 1
lfsr				.rs 8

;;;;;;; CONSTANTS ;;;;;;;;

TURN_LENGTH_FRAMES = $06

;;;;;;;;;;;;;;;
	
	.bank 0
	.org $C000 
	
TickRNG:
	LDA <lfsr
.roll
	LSR A
	ROR <lfsr+1
	ROR <lfsr+2
	ROR <lfsr+3
	ROR <lfsr+4
	ROR <lfsr+5
	ROR <lfsr+6
	ROR <lfsr+7
	BCC .done
	EOR #$A3
.done:
	DEX
	BNE .roll
	STA <lfsr
	LDA #$08
	STA lfsr_index
	RTS
	
RandByte:
	LDY lfsr_index
	DEY
	BEQ .reset
.cont
	LDA lfsr, y
	STY lfsr_index
	RTS
.reset:
	JSR TickRNG
	LDY lfsr_index
	JMP .cont

; Read controller state and store it in our buttons variable
ReadController:
; Store the previous frame's inputs
	LDA buttons
	STA buttons_old
	
	LDA #$01
	STA $4016
	LDA #$00
	STA $4016
	LDX #$08
LReadController:
	LDA $4016
	LSR A 		; sets the carry bit if our button is pressed
	ROL buttons	; rotates left, adding the carry bit stored.
	DEX
	BNE LReadController
	
; Calculate the just pressed and just released buttons
; Pressed	= new & not old
; Released	= old & not new

	LDA buttons_old
	EOR #$FF 		; NOT
	AND buttons
	STA buttons_pressed
	
	LDA buttons
	EOR #$FF
	AND buttons_old
	STA buttons_released
	
	RTS

; Wait for the next vertical blank period to start	
VBlankWait:
	BIT $2002
	BPL VBlankWait
	RTS
	
;;;;;;;;; ENTRY POINT ;;;;;;;;;
RESET:
	SEI				; disable IRQs
	CLD				; disable decimal mode
	LDX #$40
	STX $4017		; disable APU frame IRQ
	LDX #$FF
	TXS				; Set up stack
	INX				; now X = 0
	STX $2000		; disable NMI
	STX $2001		; disable rendering
	STX $4010		; disable DMC IRQs
	
	JSR VBlankWait	; First wait for vblank to make sure PPU is ready


; clear 0x0000 to 0x07FF
	LDX #$00
	LDY #$00
clrmem:
	LDA #$00
	STA $0000, x
	STA $0100, x
	STA $0300, x
	STA $0400, x
	STA $0500, x
	STA $0600, x
	STA $0700, x
	LDA #$FE
	STA $0200, x
	INX
	BNE clrmem
	 
	JSR VBlankWait		; Second wait for vblank, PPU is ready after this
	
; Initialise our timer
	LDA #TURN_LENGTH_FRAMES
	STA turn_anim_timer

; Run an RNG tick
	JSR TickRNG

; seed our rng based on graphics ram, which
; is most volatile
	LDA #$F6
	STA lfsr
	LDA #$30
	STA lfsr+1
	LDA #$3B
	STA lfsr+2
	LDA #$B2
	STA lfsr+3

LoadPalettes:
	LDA $2002 ; read PPU status, resets the high/low latch

	; Set the PPU address to $3F10
	LDA #$3F
	STA $2006
	LDA #$10
	STA $2006

; Load our palette data into the PPU starting at $3F10
	LDX #$00
LLoadPalette:
	LDA PaletteData, x
	STA $2007
	INX
	CPX #$20 ; Compare X to 0x20 (32)
	BNE LLoadPalette

LoadSprites:
	LDX #$00
LLoadSprites:
	LDA SpriteAttributeData, x
	STA $0200, x
	INX
	CPX #$10 ; Load 0x20 (32) bytes
	BNE LLoadSprites

LoadBG:
	; Set PPU Pointer to 0x2000
	LDA $2002
	LDA #$20
	STA $2006
	LDA #$00
	STA $2006
	
	LDA #LOW(Nametable)
	STA pointer_lo
	LDA #HIGH(Nametable)
	STA pointer_hi
	
	LDX #$00
	LDY #$00

; Load 1kb of background
OLLoadBG:
ILLoadBG:
	LDA [pointer_lo], y
	STA $2007
	
	INY
	CPY #$00
	BNE ILLoadBG
	
	INC pointer_hi
	INX
	CPX #$04
	BNE OLLoadBG
	
	
	LDX #$00
LLoadBG:
	LDA Nametable, x
	STA $2007
	INX
	CPX #$80 ; copy 128 bytes
	BNE LLoadBG
	
	
LoadBGAttrib:
; Set PPU Pointer to 0x23C0, beginning of background attrib table
	LDA $2002
	LDA #$23
	STA $2006
	LDA #$C0
	STA $2006
	
; Transfer 8 bytes from our ROM to the attrib table
	LDX #$00
LLoadBGAttrib:
	LDA BGAttribData, x
	STA $2007
	INX
	CPX #$08
	BNE LLoadBGAttrib

	LDA #%10010000	; enable NMI, sprites from table 0, bg from table 1
	STA $2000
	LDA #%00011110	; enable sprites, backgrounds, no clipping
	STA $2001

MainLoop:
	JMP MainLoop	 ; Wait for an NMI to begin
	
NMI:
	; Immediately begin our OAM transfer
	LDA #$00
	STA $2003
	LDA #$02
	STA $4014


;;;;;;;;; GRAPHICS UPDATES HERE ;;;;;;;;

	; Do an RNG pass every frame
	JSR RandByte

	; Read our controller state
	JSR ReadController
	
CheckTurnTimer:
	LDA turn_anim_timer
	BEQ ProcessTurnInput
	DEC turn_anim_timer
	JMP ProcessTurnInput_End
	
ProcessTurnInput:
	LDA buttons_pressed
	AND #%00001111
	BEQ ProcessTurnInput_End
	LDA #TURN_LENGTH_FRAMES
	STA turn_anim_timer
	
ReadUp:
	LDA buttons_pressed
	AND #%00001000
	BEQ ReadUp_End

	LDA #$05
	LDX #$00
LMoveUp:
	LDA $0200, x
	SEC
	SBC #$08
	STA $0200, x

	INX
	INX
	INX
	INX

	CPX #$0C
	BNE LMoveUp

ReadUp_End:

ReadDown:
	LDA buttons_pressed
	AND #%00000100
	BEQ ReadDown_End

	LDX #$00
LMoveDown:
	LDA $0200, x
	CLC
	ADC #$08
	STA $0200, x

	INX
	INX
	INX
	INX

	CPX #$0C
	BNE LMoveDown
ReadDown_End:
	
	

ReadLeft:
	LDA buttons_pressed
	AND #%00000010
	BEQ ReadLeft_End

	LDX #$00
LMoveLeft:
	LDA $0203, x
	SEC
	SBC #$08
	STA $0203, x

	INX
	INX
	INX
	INX

	CPX #$0C
	BNE LMoveLeft

ReadLeft_End:

ReadRight:
	LDA buttons_pressed
	AND #%00000001
	BEQ ReadRight_End
	
	LDX #$00
LMoveRight:
	LDA $0203, x
	CLC
	ADC #$08
	STA $0203, x

	INX
	INX
	INX
	INX

	CPX #$0C
	BNE LMoveRight
ReadRight_End:

ProcessTurnInput_End:

CleanupPPU:	
	LDA #%10010000	; enable NMI, sprites from table 0, bg from table 1
	STA $2000
	LDA #%00011110	; enable sprites, backgrounds, no clipping
	STA $2001
	LDA #$00 		; disable background scrolling
	STA $2005
	STA $2005

	RTI
 
;;;;;;;;;;;;;;	
	
	
	.bank 1
	.org $E000

; 2kb
Nametable:
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 1
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 2
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 3
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$30,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35  ;;row 4
  .db $35,$35,$35,$35,$31,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;play area top

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 5
  .db $24,$24,$24,$24,$34,$24,$0C,$12,$17,$0D,$0E,$1B,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 5
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 6
  .db $24,$24,$24,$24,$34,$24,$29,$24,$01,$09,$28,$02,$07,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 7
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 8
  .db $24,$24,$24,$24,$34,$24,$2A,$24,$00,$04,$01,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 9
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 10
  .db $24,$24,$24,$24,$34,$24,$2B,$24,$00,$02,$08,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 12
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 13
  .db $24,$24,$24,$24,$34,$24,$2C,$24,$01,$03,$07,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 14
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 15
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 16
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 17
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 18
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 19
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 20
  .db $24,$24,$24,$24,$34,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;sides

  .db $24,$24,$24,$32,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35,$35  ;;row 21
  .db $35,$35,$35,$35,$33,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;play area bottom

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 22
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 23
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 24
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 25
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 26
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 27
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 28
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 29
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky

  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;row 30
  .db $24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24,$24  ;;all sky
  
  ; 64 bytes
BGAttribData:
  .db %11000000, %11110000, %11110000, %11110000, %11110000, %00110000, %00000000, %00000000
  .db %11001100, %00000000, %00000000, %00000000, %00000000, %00110011, %00000000, %00000000
  .db %11001100, %00000000, %00000000, %00000000, %00000000, %00110011, %00000000, %00000000
  .db %11001100, %00000000, %00000000, %00000000, %00000000, %00110011, %00000000, %00000000
  .db %11001100, %00000000, %00000000, %00000000, %00000000, %00110011, %00000000, %00000000
  .db %00001100, %00001111, %00001111, %00001111, %00001111, %00000011, %00000000, %00000000
  .db %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000
  .db %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000, %00000000
  
PaletteData:
; Sprite Data
	.db $0F,$16,$27,$18,	$0F,$20,$10,$00,	$0F,$1C,$15,$14,	$0F,$02,$38,$3C
; Background Data
	.db $0F,$20,$10,$00,	$22,$36,$17,$0F,	$22,$30,$21,$0F,	$22,$27,$17,$17

  
SpriteAttributeData:
	.db $7F,$02,$00,$86
	.db $7A,$01,$02,$80
	.db $7F,$00,$01,$80

	.org $FFFA	 	; first of the three vectors starts here
	.dw NMI			; set the NMI interrupt
	.dw RESET		; set the RESET interrupt
	.dw 0			; disable the IRQ interrupt
	
	
;;;;;;;;;;;;;;	
	
	
	.bank 2
	.org $0000
	.incbin "roguelike.chr"	 ;includes 8KB graphics file from SMB1
